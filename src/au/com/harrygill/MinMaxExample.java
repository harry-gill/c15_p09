package au.com.harrygill;

import java.util.stream.Stream;

public class MinMaxExample {

    public static void main(String[] args) {

        Stream<Integer> finiteStream = Stream.of(5, 4, 2, 1, 5, 6, 7, 8, 9, 2, 1, 10, 20, 100);

        System.out.println("Max = " + finiteStream.max(Integer::compareTo).get());

        Stream<Double> infiniteStream = Stream.generate(Math::random);

        //System.out.println("Min = " + infiniteStream.min(Double::compareTo));
    }
}