package au.com.harrygill;

import java.util.stream.Stream;

public class FindExample {

    public static void main(String[] args) {

        Stream<String> finiteStream = Stream.of("Java", "World", "Hello");

        System.out.println("Any Element = " + finiteStream.findAny().get());

        Stream<Double> infiniteStream = Stream.generate(Math::random);

        System.out.println("Any Element = " + infiniteStream.findAny().get());
    }
}