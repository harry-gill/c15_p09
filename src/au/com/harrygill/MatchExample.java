package au.com.harrygill;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class MatchExample {

    public static void main(String[] args) {

        var list = List.of("monkey", "2", "chimp");
        Predicate<String> pred = x -> Character.isLetter(x.charAt(0));

        System.out.println("Is anyMatch: " + list.stream().anyMatch(pred));
        System.out.println("Is AllMatch: " + list.stream().allMatch(pred));
        System.out.println("Is nonMatch: " + list.stream().noneMatch(pred));

        Stream<String> infiniteStream = Stream.generate(() -> "chimp");
        Predicate<String> pred2 = x -> Character.isDigit(x.charAt(0));

        //System.out.println("Infinite Stream, anyMatch: " + infiniteStream.anyMatch(pred2));
        //System.out.println("Infinite Stream, nonMatch: " + infiniteStream.noneMatch(pred));
        System.out.print("Infinite Stream, allMatch: " + infiniteStream.allMatch(pred2));
    }
}