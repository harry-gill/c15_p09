package au.com.harrygill;

import java.util.stream.Stream;

public class CountExample {

    public static void main(String[] args) {

        Stream<Integer> integerStream = Stream.of(5, 4, 3, 2, 5, 6, 7, 8, 9, 0);

        System.out.println("Count = " + integerStream.count());

        Stream<Double> infiniteStream = Stream.generate(Math::random);
        //System.out.println("Count Infinite Stream: " + infiniteStream.count());
    }
}