package au.com.harrygill;

import java.util.stream.Stream;

public class ForEachExample {

    public static void main(String[] args) {

        Stream<String> stringStream = Stream.of("Java", "says", "Hello");

        stringStream.forEach(System.out::println);

        Stream<Double> infiniteStream = Stream.generate(Math::random);
        //infiniteStream.forEach(System.out::println);
    }
}